---
title: Implementation of the I2C bus: commanding an OLED display
description: Implementation of I2C bus and OLED SSD1306 display with MicroPython
---

# Implementation of the I2C bus: commanding an OLED display

This tutorial explains how to implement an I2C (or I<sup>2</sup>C) OLED display with MicroPython.
It is very easy to use an OLED screen with MicroPython to display messages. We will see in this example how to connect the screen to the I2C bus, then how to command it to send messages with MicroPython. For the example, we will use the OLED monochrome screen, 192 x 32 pixels from Adafruit, however all screens integrating the SSD1306 controller are compatible.

## What is I2C?

[I<sup>2</sup>C](https://en.wikipedia.org/wiki/I2C) stands for "Inter-Integrated Circuit". It is a serial bus operating according to a protocol invented by Philips. To chat on an I2C bus, each "slave" module connected to it **is identified by a 7-bit-coded address** in order to chat with the master controller integrated in the STM32WB55.

With the proliferation of I2C modules, **it could happen that two or more modules that you connected on an I2C bus have the same address**, which would inevitably lead to crashes. To avoid this type of conflict, you should consult the datasheets of your modules and, for those that allow it, take care to modify (if necessary) their I2C address, usually encoded in their firmware.

The maximum number of devices is therefore limited by the number of available addresses, 7 addressing bits and an R/W bit (read or write), so 128 devices, but it also depends on the bus capacity (on which the maximum bus speed depends).

The I2C bus uses **4 pins**: two for power supply (GND and V<sub>DD</sub>) and two for communication: 
 - SDA (Serial Data Line): two-way data line,
 - SCL (Serial Clock Line): two-way synchronization clock line.

These 2 lines are drawn at voltage level V<sub>DD</sub> through the **pull-up** mode.

More information can be found on [**the I2C Wikipedia page**](https://en.wikipedia.org/wiki/I2C).


## Required Hardware

1. NUCLEO-WB55 Board
3. A display with an SSD1306 controller


Here’s how to connect the OLED screen:
* D15=SCL
* D14=SDA

<div align="center">
<img alt="oled" src="images/oled.png" width="400px">
</div>

## MicroPython code

**The following scripts are available in [the download area](https://stm32python.gitlab.io/fr/assets/Script/NUCLEO_WB55.zip)**. Once you have finished downloading, you will need to transfer the file to the *PYBLASH* device directory.

```python
# Objet of the script :
# Write a text on an OLED display controlled by an SSD1306.
# I2C bus implementation demonstration.

from machine import Pin, I2C # Classes for controlling input-output and I2C bus
import ssd1306 # Classe to control the display
from time import sleep_ms

# I2C device Initialization 
i2c = I2C(1)

# Pause for a second to allow the I2C to initialize
sleep_ms(1000)

# Setting the screen characteristics
largeur_ecran_oled = 128
longueur_ecran_oled = 32
oled = ssd1306.SSD1306_I2C(largeur_ecran_oled, longueur_ecran_oled, i2c)

# Sending text to display on the OLED screen
oled.text('MicroPython OLED!', 0, 0)
oled.text('    I2C   ', 0, 10)
oled.text('Easy Peasy !!!', 0, 20)
oled.show()
```
