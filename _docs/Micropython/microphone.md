---
title: Recording an audio file on a micro-SD card
description: Recording of an audio file on a micro-SD card
---

### Recording an audio file on a micro-SD card

In this chapter we will see two new features, recording an audio file in wav format and saving the file to the SD card.

![SDCard](images/microphone_1.png)

_The Flash memory size of the nucleo STM32WB55 card being limited, the audio function is dependent on the recording functionality on SD card._

To complete this tutorial you will need to have:

1. From the latest micropython firmware in revision 4 and having updated the Nucleo board.
2. A microphone from the STEVAL-MIC003V1, 1V1 or 2V1 expansion card.
![Micro STEVAL-MIC003V1](images/microphone_2.png)
3. Support for micro SD and SD card. (https://www.adafruit.com/product/254)
![SDCard support](images/microphone_3.png)

Here is how to wire the different components with the Nucleo board:

![Fritzing montage](images/microphone_4.png)

Once the wiring has been completed, you can now record an audio file on the µSD card.

For that here are the python commands to use:

``` python
import pyb

audio = pyb.Sai () #Initialize the SAI audio device

# The record method takes 2 parameters
# 1: The name of the File.
# 2: The recording time in number of seconds

audio.record ('test_audio.wav', 5)
```

_It is recommended to reconnect the NUCLEO card to re-synchronize the files present with WIndows._

The audio file 'test_audio.wav' should then appear in the list of files stored on the SD card.
